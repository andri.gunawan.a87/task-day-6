from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("login", views.user_login, name="login"),
    path("register", views.register, name="register"),
    path("logout", views.user_logout, name="logout"),
    path("lowongan", views.lowongan, name="lowongan"),    
    path("add_job", views.add_job, name="add_job"),
    path("edit_job/<int:job_id>", views.edit_job, name="edit_job"),
    path("delete_job/<int:job_id>", views.delete_job, name="delete_job"),
    path("job_detail/<int:job_id>", views.job_detail, name="job_detail"),
    path("all_jobs", views.all_jobs, name="all_jobs"),
    path("job_apply/<int:job_id>", views.job_apply, name="job_apply"),    
    path("list_pelamar", views.list_pelamar, name="list_pelamar"),  
    path("detail_lamaran/<int:lamaran_id>", views.detail_lamaran, name="detail_lamaran"),
    path("proses_lamaran/<int:lamaran_id>", views.proses_lamaran, name="proses_lamaran"),
    path("lowongan_saya", views.lowongan_saya, name="lowongan_saya"),
    path("notifikasi_lamaran/<int:lamaran_id>", views.notifikasi_lamaran, name="notifikasi_lamaran"),
    path("status_lamaran/<int:lamaran_id>", views.status_lamaran, name="status_lamaran"),
    path("list_test", views.list_test, name="list_test"),
    path("detail_test/<int:lamaran_id>", views.detail_test, name="detail_test"),
    path("hasil_test", views.hasil_test, name="hasil_test"),
    path("data_pelamar_csv", views.data_pelamar_csv, name="data_pelamar_csv"),
    path("user_pdf/<int:pk>", views.user_pdf.as_view(), name="user_pdf")

]