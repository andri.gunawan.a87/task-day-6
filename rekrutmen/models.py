# Create your models here.
from django.db import models
from django.contrib.auth.models import User, AbstractUser

# Create your models here.

# Create your models here.
class Roles(models.Model):
    role_choice = [
        ('h', 'HCD'),
        ('p', 'Pelamar'),
        ('k', 'Korektor'),
    ]
    # ID = models.IntegerField()
    roles = models.CharField(max_length = 1, choices=role_choice)
    def __str__(self):
        return f"{self.get_roles_display()}"
        
class User(AbstractUser):
    user_role = models.ForeignKey(Roles, on_delete=models.CASCADE, related_name="user_role")
    
class Profile(models.Model):
    user = models.OneToOneField(to=User, on_delete=models.CASCADE, related_name="user_profile")    
    no_telepon = models.CharField(max_length = 32)
    pendidikan = models.CharField(max_length = 32)
    cv = models.FileField(upload_to='cv/', blank=True)
    def __str__(self):
        return f"{self.id} {self.user.username}"
        
class Lowongan(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_lowongan")
    posisi = models.CharField(max_length = 32)
    deskripsi = models.TextField()
    requirements = models.JSONField()
            
class Lamaran(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_lamaran")
    lowongan = models.ForeignKey(Lowongan, on_delete=models.CASCADE, related_name="job_lamaran")
    surat_lamaran = models.TextField(default=None)
    
class Test(models.Model):
    lamaran = models.ForeignKey(Lamaran, on_delete=models.CASCADE, related_name="test_lamaran")
    psikologi = models.TextField()
    teknis = models.TextField()
    coderbyte = models.TextField()
    
class Jawaban_test(models.Model):
    test = models.ForeignKey(Test, on_delete=models.CASCADE, related_name="jawaban_test")
    psikologi = models.TextField()
    teknis = models.TextField()
    coderbyte = models.TextField()
    
class Hasil_test(models.Model):
    test = models.ForeignKey(Test, on_delete=models.CASCADE, related_name="user_hasil")
    lamaran = models.ForeignKey(Lamaran, on_delete=models.CASCADE, related_name="hasil_test", null=True)
    psikologi_response = models.BooleanField(default=False)
    teknis_response = models.BooleanField(default=False)
    coderbyte_response = models.BooleanField(default=False)
    komentar = models.CharField(max_length = 256)

class Notifikasi(models.Model):
    lamaran = models.ForeignKey(Lamaran, on_delete=models.CASCADE, related_name="notifikasi_lamaran")
    lulus = models.BooleanField()