from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from .forms import RegForm, AddJobForm, BiodataForm, TestLamaranForm, JawabanTestForm, HasilTestForm
from django.contrib import messages
from .models import User, Lowongan, Lamaran, Notifikasi, Profile, Test, Jawaban_test, Hasil_test
import csv
from easy_pdf.views import PDFTemplateResponseMixin
from django.views.generic import DetailView


# Create your views here.
@login_required
def index(request):
    user_id = User.objects.get(id=3)
    return render(request, "index.html", {
        "user_id": user_id
    })
    
        
def user_login(request):
    if request.method == "POST":

        # Attempt to sign user in
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)

        # Check if authentication successful
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse("index"))
        else:
            return render(request, "login.html", {
                "message": "Salah username dan/atau password."
            })
    else:
        return render(request, "login.html", {
        })
        
def register(request):
    if request.method == "POST":
        data = RegForm(request.POST)
        if data.is_valid():
            valid_data = data.save(commit = False)
            valid_data.set_password(valid_data.password)
            valid_data.save()
            login(request, valid_data)
            messages.success(request, 'Pendaftaran Berhasil')
            return HttpResponseRedirect(reverse("index"))
        else:
            return render(request, "register.html", {'form':data})
    else:
        form = RegForm()
        return render(request, "register.html", {
            "form": form
        })

def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse("index"))

def lowongan(request):
    user_id = request.user
    list_lowongan = Lowongan.objects.filter(user = user_id)
    return render(request, "lowongan.html", {
        "data": list_lowongan
    })
    
def add_job(request):
    if request.method == "POST":
        user_id = request.user
        data = AddJobForm(request.POST)
        req = request.POST.getlist("req")
        if data.is_valid():
            valid_data = data.save(commit = False)
            valid_data.user = user_id
            valid_data.requirements = req
            valid_data.save()
            messages.success(request, 'Lowongan berhasil ditambahkan')
            return HttpResponseRedirect(reverse("lowongan"))
        else:
            return render(request, "add_job.html", {
            'form':data,
            'message': "Data tidak valid"
            })
                
    else:
        form = AddJobForm()
        return render(request, "add_job.html", {
            "form": form,
            "message": data.id
        })
        
def edit_job(request, job_id):
    data = Lowongan.objects.get(id = job_id)
    form = AddJobForm(instance = data)
    if request.method == "POST":
        form = AddJobForm(request.POST, instance = data)
        if form.is_valid():
            valid_data = form.save(commit = False)
            valid_data.user = user_id
            valid_data.requirements = req
            valid_data.save()
            messages.success(request, 'Lowongan Berhasil Diubah')
            return HttpResponseRedirect(reverse("lowongan"))
        else:
            messages.error(request, 'Data tidak valid')
            return render(request, "add_job.html", {
                "form":form,
            })
    else:
        return render(request, "add_job.html", {
            "form": form,
            "data": data,
            "message": data.posisi

        })

def delete_job(request, job_id):
    if request.method == "POST":
        job_data = Lowongan.objects.get(id = job_id)
        job_data.delete()
        messages.success(request, 'Buku berhasil dihapus')
        return HttpResponseRedirect(reverse("lowongan"))
        
def job_detail(request, job_id):
    data = Lowongan.objects.get(id = job_id)
    return render(request, "job_detail.html", {
        "data": data,
    })

def all_jobs(request):
    data = Lowongan.objects.all()
    return render(request, "all_jobs.html", {
        "data": data,
    })
    
def job_apply(request, job_id):
    lowongan_data = Lowongan.objects.get(id = job_id)
    user_id = request.user
    if request.method == "POST":
        try:
            data_profile = Profile.objects.get(user = user_id)
            data = BiodataForm(request.POST, request.FILES, instance = data_profile)
        except:
            data = BiodataForm(request.POST, request.FILES)
        if data.is_valid():
            user_id.first_name = request.POST["firstname"]
            user_id.last_name = request.POST["lastname"]
            user_id.save()
            valid_data = data.save(commit = False)
            valid_data.user = user_id
            valid_data.save()
            surat_lamaran = request.POST["surat_lamaran"]
            lamaran = Lamaran (user = user_id, lowongan = lowongan_data, surat_lamaran = surat_lamaran)
            lamaran.save()
            messages.success(request, 'Pekerjaan berhasil dilamar')
            return HttpResponseRedirect(reverse("index"))
        else:
            return render(request, "job_apply.html", {
            'form':data,
            'message': data.errors,
            'job': lowongan
                })
    else:
        user_data = Profile.objects.get(user = user_id)
        form = BiodataForm(instance = user_data)
        return render(request, "job_apply.html", {
            "form": form,
            'job': lowongan_data
        })

def list_pelamar(request):
    user_id = request.user
    list = Lowongan.objects.filter(user = user_id)
    para_pelamar = Lamaran.objects.filter(lowongan__in = list)
    return render(request, "list_pelamar.html", {
        "data": para_pelamar
    })
    
def detail_lamaran(request, lamaran_id):
    if request.method == "POST":
        data_lamaran = Lamaran.objects.get(id = lamaran_id)
        body = request.POST.get("proses", "tolak")
        if "proses" in request.POST:
            try:
                notifikasi = Notifikasi.objects.get(lamaran = data_lamaran)
                notifikasi.lulus = True
                messages.success(request, "Status Lamaran berhasil diupdate 'Lulus'")
            except:
                notifikasi = Notifikasi(lulus = True, lamaran = data_lamaran)
                messages.success(request, "Status Lamaran 'Lulus'")
            notifikasi.save()
            return redirect("proses_lamaran", lamaran_id = lamaran_id)
        else:
            try:
                notifikasi = Notifikasi.objects.get(lamaran = data_lamaran)
                notifikasi.lulus = False
                messages.success(request, "Status Lamaran berhasil diupdate 'Tidak Lulus'")
            except:
                notifikasi = Notifikasi(lulus = False, lamaran = data_lamaran)
                messages.success(request, "Status Lamaran 'Tidak Lulus'")
            notifikasi.save()
            return redirect("notifikasi_lamaran", lamaran_id = lamaran_id)
    else:
        data = Lamaran.objects.get( id = lamaran_id)
        return render(request, "detail_lamaran.html", {
            "data": data
        })
    
def proses_lamaran(request, lamaran_id):
    data = Lamaran.objects.get(id = lamaran_id)
    test = Test.objects.get(lamaran = data)
    if request.method == "POST":
        user_id = request.user
        try:
            form = TestLamaranForm(request.POST, instance=test)
            messages.success(request, "Test Lamaran Berhasil Diupdate")
        except:
            form = TestLamaranForm(request.POST)
            messages.success(request, "Test Lamaran Berhasil Dibuat")
        if form.is_valid():
            valid_data = form.save(commit = False)
            valid_data.user = user_id
            valid_data.lowongan = data.lowongan
            valid_data.lamaran = data
            valid_data.save()
            return redirect("notifikasi_lamaran", lamaran_id = lamaran_id)
        else:
            return render(request, "proses_lamaran.html", {
                "form": form,
                "data": data
            })
    else:
        try:
            form = TestLamaranForm(instance=test)
        except:
            form = TestLamaranForm()
        return render(request, "proses_lamaran.html", {
            "form": form,
            "data": data
        })
        
def lowongan_saya(request):
    user_id = request.user
    data = Lowongan.objects.filter(job_lamaran__user = user_id)
    return render(request, "lowongan_saya.html", {
        "data": data,
    })
    
def notifikasi_lamaran(request, lamaran_id):
    data = Lamaran.objects.get(id = lamaran_id)
    return render(request, "notifikasi_lamaran.html", {
        "data": data,
    })
    
def status_lamaran(request, lamaran_id):
    data = Lamaran.objects.get(id = lamaran_id)
    soal_test = Test.objects.get(lamaran = data)
    if request.method == "POST":
        form = JawabanTestForm(request.POST)
        if form.is_valid():
            valid_data = form.save(commit = False)
            valid_data.test = soal_test
            valid_data.save()
            messages.success(request, 'Jawaban Test Berhasil disubmit')
            return HttpResponseRedirect(reverse("lowongan_saya"))
        else:
            messages.error(request, 'Data tidak valid')
            return render(request, "add_job.html", {
                "form":form,
            })
    else:
        try:
            form_data = Jawaban_test.objects.get(test = soal_test)
            form = JawabanTestForm(instance = form_data)
        except:
            form = JawabanTestForm()
        return render(request, "status_lamaran.html", {
            "data": data,
            "soal": soal_test,
            "form": form
        })

def list_test(request):
    data = Notifikasi.objects.filter(lulus = True)
    return render(request, "list_test.html", {
        "data": data,
    })
    
def detail_test(request, lamaran_id):
    data_lamaran = Lamaran.objects.get(id = lamaran_id)
    data_test = Test.objects.get(lamaran = data_lamaran)
    form_data = Hasil_test.objects.get(lamaran = data_lamaran)
    if request.method == "POST":
        try:
            form = HasilTestForm(request.POST, instance = form_data)
        except:
            form = HasilTestForm(request.POST)
        if form.is_valid():
            valid_data = form.save(commit = False)
            valid_data.test = data_test
            valid_data.lamaran = data_lamaran
            valid_data.save()
            messages.success(request, 'Hasil Test Berhasil disubmit')
            return HttpResponseRedirect(reverse("list_test"))
        else:
            messages.error(request, 'Data tidak valid')
            return render(request, "add_job.html", {
                "form":form,
            })
    else:
        jawaban_test = Jawaban_test.objects.get(test = data_test)
        try:
            form = HasilTestForm(instance = form_data)
        except:
            form = HasilTestForm()
        return render(request, "detail_test.html", {
            "data": jawaban_test,
            "soal": data_test,
            "form": form
        })
        
def hasil_test(request):
    user_id = request.user
    data = Hasil_test.objects.filter(lamaran__lowongan__user = user_id)
    return render(request, "hasil_test.html", {
        "data": data
    })
    
def data_pelamar_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="data_pelamar.csv"'
    writer = csv.writer(response)
    writer.writerow(['Username', 'Nama Depan', 'Nama Belakang', 'Posisi', 'Pendidikan', 'No Telepon', 'Email'])
    datas = Lamaran.objects.all().values_list('user__username', 'user__first_name','user__last_name', 'lowongan__posisi', 
                'user__user_profile__pendidikan', 'user__user_profile__no_telepon', 'user__email')
    for data in datas:
        writer.writerow(data)
    return response
    
class user_pdf(PDFTemplateResponseMixin, DetailView):
    template_name = 'user_pdf.html'
    context_object_name = "data"
    model = Lamaran