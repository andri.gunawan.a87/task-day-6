from django import forms
from .models import User, Lowongan, Lamaran, Test, Profile, Jawaban_test, Hasil_test

class RegForm(forms.ModelForm):
    password=forms.CharField(widget=forms.PasswordInput())
    confirm_password=forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model = User
        fields = ["username", "email", "user_role", "password"]
        
    def clean(self):
        super(RegForm, self).clean()
        password = self.cleaned_data.get("password")
        confirm_password = self.cleaned_data.get("confirm_password")
        if password != confirm_password:
            self._errors['password'] = self.error_class([
                'Password dan konfirmasi password tidak sesuai'])
        return self.cleaned_data
        
    def __init__(self, *args, **kwargs):
        super(RegForm, self).__init__(*args, **kwargs)

class AddJobForm(forms.ModelForm):
    class Meta:
        model = Lowongan
        fields = ["user", "posisi", "deskripsi", "requirements"]
        
    def clean(self):
        super(AddJobForm, self).clean()
        return self.cleaned_data
        
    def __init__(self, *args, **kwargs):
        super(AddJobForm, self).__init__(*args, **kwargs)
        self.fields['requirements'].required = False
        self.fields['user'].required = False

class BiodataForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ["user", "pendidikan", "no_telepon", "cv",]
        
    def clean(self):
        super(BiodataForm, self).clean()
        return self.cleaned_data
        
    def __init__(self, *args, **kwargs):
        super(BiodataForm, self).__init__(*args, **kwargs)
        self.fields['user'].required = False
        
class TestLamaranForm(forms.ModelForm):
    class Meta:
        model = Test
        fields = ["psikologi", "teknis", "coderbyte", "lamaran"]
        
    def clean(self):
        super(TestLamaranForm, self).clean()
        return self.cleaned_data
    
    def __init__(self, *args, **kwargs):
        super(TestLamaranForm, self).__init__(*args, **kwargs)
        self.fields['lamaran'].required = False
        
class JawabanTestForm(forms.ModelForm):
    class Meta:
        model = Jawaban_test
        fields = ["psikologi", "teknis", "coderbyte", "test"]
        
    def clean(self):
        super(JawabanTestForm, self).clean()
        return self.cleaned_data
    
    def __init__(self, *args, **kwargs):
        super(JawabanTestForm, self).__init__(*args, **kwargs)
        self.fields['test'].required = False
        
class HasilTestForm(forms.ModelForm):
    class Meta:
        model = Hasil_test
        fields = ["psikologi_response", "teknis_response", "coderbyte_response", "komentar"]
        
    def clean(self):
        super(HasilTestForm, self).clean()
        return self.cleaned_data
    
    def __init__(self, *args, **kwargs):
        super(HasilTestForm, self).__init__(*args, **kwargs)